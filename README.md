# Your project
```
* Author: NhuanPT
* Created date: 14/08/2019
* Version: 1.0.0
```
 - [Keywords](#markdown-header-keywords)
 - [Descriptions](#markdown-header-descriptions)
 - [Requirements](#markdown-header-requirements)
 - [How to run](#markdown-header-how-to-run)
 - [Project architectures](#markdown-header-project-architectures)
 - [Famous features](#markdown-header-famous-features)
 - [References](#markdown-header-references)
  
## Keywords
`Required; MAX: 3 Keywords; Language: EN`

1. Từ khóa 1 
2. Từ khóa 2 
3. Từ khóa 3 

## Descriptions
`Mô tả theo từng ý`

1. Mô tả số 1 
1. Mô tả số 2 
1. Mô tả số 3 
1. ... 

## Requirements
`Yêu cầu kiến thức; Yêu cầu phần mềm`

1. Yêu cầu 1
1. Yêu cầu 2
1. ...

## How to run
`Hướng dẫn để chạy được source này`
#### Step 1
Hướng dẫn bước 1
#### Step 2
Hướng dẫn bước 2

## Project architectures
`Luồng chạy của chương trình từ INPUT => OUTPUT`
`TEXT or IMAGE`

![Alt text](doc/mvc.png)

## Famous features
`Mô tả các tính năng nội bật trong project`
#### Tính năng 1
Mô tả sơ lược tính năng 1
####  Tính năng 2
Mô tả sơ lược tính năng 2
## References
`Mô tả tài liệu tham khảo`

1. [Website 1](http://website1.com) 
1. [Tài liệu trên server](172.1.1.1) 