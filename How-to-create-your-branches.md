# How to create your branches
```
* Author: Duong Truong Nhat
* Created date: 16/08/2019
* Version: 1.0.1
```
 - [Repositories](#markdown-header-repositories)
 - [How to create new your branches](#markdown-header-how-to-create-new-your-branches)
 - [How to extend existing branch](#markdown-header-how-to-extend-existing-branch)
  
## Repositories
Leader đã tạo 07 repo tương ứng với 07 sprint.

```
0. Java_Training_Sprint_0
1. Java_Training_Sprint_1
2. Java_Training_Sprint_2
3. Java_Training_Sprint_3
4. Java_Training_Sprint_4
5. Java_Training_Sprint_5
6. Java_Training_Sprint_6
```
![picture alt](doc/list-of-repos.png "Title is optional")
> <b>Hình 1:</b> Danh sách repositories đã được tạo

## How to create new your branches
> <b>Syntax:</b> `[id]_[employee_no]_[name]_[labid]`

```
Chú ý: chữ thường, dấu _
* id: thứ tự của Spritn (0, 1, 2, 3, 4, 5, 6)
* employee_no: mã nhân viên tại Công ty Lampart
* name: tên của nhân viên
* labid: lab + id(thứ tự bài lab)
* Trong 1 Sprint có thể có nhiều lab
```

![picture alt](doc/list-of-branches.png "Title is optional")

> <b>Hình 2:</b> Danh sách branches đã được tạo trong repositories Java_Training_Sprint_1

## How to extend existing branch
> <b>Syntax:</b> `[id]_[employee_no]_[name]_[labid]_extend_[existing_branch]`

```
Chú ý: chữ thường, dấu _
* Tương tự như tạo mới branch
* existing_branch: là tên branch hiện có (copy/paste)
```
